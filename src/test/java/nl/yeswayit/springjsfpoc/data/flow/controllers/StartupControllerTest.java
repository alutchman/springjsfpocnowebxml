package nl.yeswayit.springjsfpoc.data.flow.controllers;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@Slf4j
@RunWith(MockitoJUnitRunner.class)
public class StartupControllerTest {
    public static final String APPNAME= "SOME_APP";
    @Mock
    private ApplicationContext applicationContext;

    @InjectMocks
    private StartupController startupController;

    @Before
    public void init(){
        when(applicationContext.getApplicationName()).thenReturn(APPNAME);
    }

    @Test
    public void loginGet() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        MockHttpServletResponse response = new MockHttpServletResponse();
        ModelAndView model = startupController.loginGet(request, response);

        assertNotNull(model);
        assertEquals("index", model.getViewName());
        assertEquals("SOME_APP", model.getModelMap().get("baseUrl"));
    }
}
