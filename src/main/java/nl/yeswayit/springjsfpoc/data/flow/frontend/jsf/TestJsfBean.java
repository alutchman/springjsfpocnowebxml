package nl.yeswayit.springjsfpoc.data.flow.frontend.jsf;

import nl.yeswayit.springjsfpoc.data.flow.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@Component
@RequestScoped
@ManagedBean
public class TestJsfBean {
    @Autowired
    private MessageService messageService;

    public String getMsg() {
        return messageService.getMsg();
    }
}