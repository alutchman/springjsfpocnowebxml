package nl.yeswayit.springjsfpoc.data.flow.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
public class StartupController {
    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping(value="/", method = RequestMethod.GET)
    public ModelAndView loginGet(HttpServletRequest request, HttpServletResponse response){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        log.info("Main page loaded");
        return new ModelAndView("index",datamap);
    }
}
