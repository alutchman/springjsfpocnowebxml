package nl.yeswayit.springjsfpoc.config;

import com.sun.faces.config.ConfigureListener;
import com.sun.faces.config.FacesInitializer;
import lombok.extern.slf4j.Slf4j;
import nl.yeswayit.springjsfpoc.config.MySpringBeanConfig;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

@Slf4j
public class NoWebXmlConfig extends FacesInitializer implements WebApplicationInitializer {

    private AnnotationConfigWebApplicationContext getContext() {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.setConfigLocation("nl.yeswayit.springjsfpoc.config");
        context.register(MySpringBeanConfig.class);
        log.info("............... Created AnnotationConfigWebApplicationContext...................");
        return context;
    }


    /**
     * gets invoked automatically when application starts up
     *
     * @param servletContext
     * @throws ServletException
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        //============================================VERY IMPORTANT OTHERWISE APP UNSTABLE================
        servletContext.setInitParameter("com.sun.faces.forceLoadConfiguration", Boolean.TRUE.toString());
        //=================================================================================================
        WebApplicationContext context = getContext();
        servletContext.addListener(new ContextLoaderListener(context));

        ServletRegistration.Dynamic dispatcher =
                servletContext.addServlet("DispatcherServlet", new DispatcherServlet(context));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");


        log.info("............... Created WebApplicationContext...................");


        // Use JSF view templates saved as *.xhtml, for use with Facelets
        servletContext.setInitParameter("javax.faces.DEFAULT_SUFFIX", ".xhtml");
        // Enable special Facelets debug output during development
        servletContext.setInitParameter("javax.faces.PROJECT_STAGE","Development");
        servletContext.addListener(ConfigureListener.class);

        ServletRegistration.Dynamic facesServlet = servletContext.addServlet("Faces Servlet", FacesServlet.class);
        facesServlet.setLoadOnStartup(1);
        facesServlet.addMapping("*.jsf","*.xhtml");
        log.info("............... Loaded FacesServlet...................");

        //Add OpenEntityManagerInViewFilter Filter
        //servletContext.addFilter("openEntityManagerInViewFilter",
        //OpenEntityManagerInViewFilter.class).addMappingForUrlPatterns(null, true, "/*");
    }
}